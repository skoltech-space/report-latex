# To use this template:

1. Rename report.tex into something else. Proposed form
      PROJECTNAME-PHASE-WORKPACKAGE-LASTNAME-yourworkname.tex
    EXAMPLE:
	CubETH-C-ADCS-Ivanov-KalmanFilterEstimates.tex

2. To compile fully file run:

   ```
   xelatex $name.tex
   bibtex $name
   makeglossaries $name
   xelatex $name
   ```
   
   You only need to run bibtex and makeglossaries once, after you have
   introduced new reference items or acronyms.

   or just use `latexmk report.tex`

# To set continuous deployment to google drive:

1. Turn on pipelines for your project
2. Set up variables
     `GDRIVE_REFRESH_TOKEN` and `GDRIVE_DIR` in GitLab CI/CD Settings
3. Modify .gitlab-ci.yml to grab your renamed file

## Google Drive Folder ID

Go to Google Drive and navigate to the folder where you wish artifacts to be uploaded. Folder id is a string following `folders/` in the url

## Google Drive Refresh Token

Download the `prasmussen/gdrive` binary for your system and run the following command.

```
$ ./gdrive list
Authentication needed
Go to the following url in your browser:
https://accounts.google.com/o/oauth2/auth?access_type=offline&client_id=...

Enter verification code: ...
Id                             Name                                       Type   Size      Created
0B1h7GlNyl9MaSnNMY0RRWW5PREU   app-client-feature-...3689705bfe.tar.bz2   bin    8.0 MB    2016-07-11 22:25:17
0B1h7GlNyl9MaWE8ybFk1SjROMWc   app-server-master-2...34b0e75e79.tar.bz2   bin    16.3 MB   2016-07-11 22:22:59
```
It will ask you to authenticate at a Google URL. Go to the URL, login with your Google Account, permit the app to view/edit your Google Drive files and copy/paste the verification code you’ve received back into the command line prompt.

Notice that when you run ./gdrive list a second time it no longer prompts you for authentication. This is because the client has cached your OAuth2 access tokens and refresh tokens in the file ~/.gdrive/token_v2.json:
```
{
  "access_token": "...",
  "token_type": "Bearer",
  "refresh_token": "...",
  "expiry": "2016-07-13T11:47:00.424973987-04:00"
}
```
